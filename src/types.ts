export type SearchContextType = {
  search: string;
  setSearch: (value: string) => void;
};

export interface TableOfContentsProps {
  sections: {
    toclevel: number;
    line: string;
    number: string;
  }[];
}

export interface CategoriesProps {
  categories: {
    category: string;
    hidden: boolean;
  }[];
}
