import { createContext } from "react";
import { SearchContextType } from "../types";

export const SearchContext = createContext<SearchContextType>({
  search: "",
  setSearch: (search) => null,
});
