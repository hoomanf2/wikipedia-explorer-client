import React from "react";
import { TableOfContentsProps } from "../types";
import "./TableOfContents.css";

function TableOfContents({ sections }: TableOfContentsProps) {
  return (
    <div className="sections">
      <h3>Sections</h3>
      <ul>
        {sections.map((item, index) => (
          <li
            className={item.toclevel > 1 ? `toc-level-${item.toclevel}` : ""}
            key={index}
          >
            <span className="number">{item.number} </span>
            {item.line}
          </li>
        ))}
      </ul>
    </div>
  );
}

export default TableOfContents;
