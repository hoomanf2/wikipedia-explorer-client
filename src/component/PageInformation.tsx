import React, { useContext, useEffect } from "react";
import { SearchContext } from "../context/SearchContext";
import TableOfContents from "./TableOfContents";
import Category from "./Category";
import { useLazyQuery } from "@apollo/client";
import { GET_ARTICLE } from "../query";

function PageInformation() {
  const { search } = useContext(SearchContext);
  const [getArticle, { loading, data }] = useLazyQuery(GET_ARTICLE);

  useEffect(() => {
    getArticle({
      variables: { page: search },
    });
  }, [getArticle, search]);

  if (loading) return <h3>Loading...</h3>;

  if (data) {
    const article = data.getArticle;
    return (
      <div className="info">
        <h2>Page: {article.title}</h2>
        <Category categories={article.categories} />
        <TableOfContents sections={article.sections} />
      </div>
    );
  }

  return null;
}

export default PageInformation;
