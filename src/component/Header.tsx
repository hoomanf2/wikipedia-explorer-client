import React from "react";
import SearchBox from "./SearchBox";
import "./Header.css";

function Header() {
  return (
    <div className="header">
      <h1>Explore Wikipedia <span className="emoji">🔍</span></h1>
      <SearchBox />
    </div>
  );
}

export default Header;
