import React, { useContext } from "react";
import { SearchContext } from "../context/SearchContext";
import "./SearchBox.css";

function SearchBox() {
  const { search, setSearch } = useContext(SearchContext);
  return (
    <input
      className="search-box"
      type="text"
      placeholder="Search for anything you like..."
      defaultValue={search}
      onChange={(e) => setSearch(e.target.value)}
    />
  );
}

export default SearchBox;
