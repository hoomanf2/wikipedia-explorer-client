import React from "react";
import { CategoriesProps } from "../types";
import "./TableOfContents.css";

function Category({ categories }: CategoriesProps) {
  return (
    <div className="categories">
      <h3>Categories</h3>
      <ul>
        {categories.map(
          (item, index) => !item.hidden && <li key={index}>{item.category}</li>
        )}
      </ul>
    </div>
  );
}

export default Category;
