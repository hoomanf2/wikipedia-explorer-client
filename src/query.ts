import { gql } from "@apollo/client";

export const GET_ARTICLE = gql`
  query GetArticle($page: String!) {
    getArticle(page: $page) {
      title
      categories {
        category
        hidden
      }
      sections {
        toclevel
        number
        line
      }
    }
  }
`;
