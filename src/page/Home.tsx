import React from "react";
import Header from "../component/Header";
import PageInformation from "../component/PageInformation";
import "./Home.css";

function Home() {
  return (
    <div className="container">
      <Header />
      <PageInformation />
    </div>
  );
}

export default Home;
