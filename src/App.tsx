import React, { useState } from "react";
import Home from "./page/Home";
import { SearchContext } from "./context/SearchContext";

function App() {
  const [search, setSearch] = useState("");

  return (
    <SearchContext.Provider value={{ search, setSearch }}>
      <Home />
    </SearchContext.Provider>
  );
}

export default App;
